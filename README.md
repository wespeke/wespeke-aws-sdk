WeSpeke JS Libraries
===============================

## What is it?

- wespeke-aws-sdk is a project for hosting JavaScript libraries (and other web assets as needed).  This can be done
when there is no easy way to install a library for some reason.
- The project was originally named wespeke-aws-sdk as a means to host Amazon JS libraries, but we started using the
repository more generically.  We can re-name it to something like wespeke-js-lib at some point if we want.

## Build Environment Notes
- We converted from Bower to NPM, but some libraries weren't published to NPM, and had no package.json.  For those, we
copied the library source and committed into our project wespeke-aws-sdk.
    - ionic 1.3.5 was copied from [https://github.com/ionic-team/ionic-bower.git#v1.3.5](https://github.com/ionic-team/ionic-bower.git#v1.3.5)
    - wav-audio-encoder was copied from [https://github.com/higuma/wav-audio-encoder-js.git](https://github.com/higuma/wav-audio-encoder-js.git)